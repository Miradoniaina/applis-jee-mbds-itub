/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.jsf.backingbean;

import applisjee.mbds.itub.ejb.entity.Question;
import applisjee.mbds.itub.ejb.entity.Reponse;
import applisjee.mbds.itub.ejb.entity.Utilisateur;
import applisjee.mbds.itub.ejb.session.QuestionManager;
import applisjee.mbds.itub.ejb.session.ReponseManager;
import applisjee.mbds.itub.ejb.session.UtilisateurManager;
import applisjee.mbds.itub.jsf.util.Util;
import java.io.Serializable;
import java.util.Date;
import javax.ejb.EJB;
import javax.faces.context.ExternalContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Andrianina_pc
 */
@Named(value = "reponseBean")
@ViewScoped
public class ReponseBean implements Serializable {

    @EJB
    private UtilisateurManager utilisateurManager;

    @EJB
    private ReponseManager reponseManager;

    @EJB
    private QuestionManager questionManager;

    @Inject
    private ExternalContext externalContext;

    private Long questionId;
    private Question question;

    private final Reponse reponse = new Reponse();

    /**
     *
     * @return
     */
    public Reponse getReponse() {
        return reponse;
    }

    /**
     *
     * @return
     */
    public Long getQuestionId() {
        return questionId;
    }

    /**
     *
     * @return
     */
    public Question getQuestion() {
        return question;
    }

    /**
     *
     * @param question
     */
    public void setQuestion(Question question) {
        this.question = question;
    }

    /**
     *
     * @param questionId
     */
    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }
    
    /**
     * 
     * reload la question concerné
     * rempli l'attribut question de l'objet réponse par cette question
     */
    public void loadQuestion() {
        question = questionManager.findById(questionId);
        reponse.setQuestion(question);
    }

    /**
     * 
     * Rempli les attributs de l'objet reponse 
     * récupère l'utilisateur connecter pour l'attribut utilisateur de reponse
     * save reponse dans la bdd
     * sort le popup pour signifier a l'utilisateur que la reponse a été crée
     * redirige vers les détails de la question concerné
     * @return
     */
    public String repondre() {
        reponse.setDateReponse(new Date());
        reponse.setVote(0);
        
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        String pseudo = request.getUserPrincipal().getName();
        Utilisateur userConnecter = utilisateurManager.Find(pseudo);
        reponse.setUtilisateur(userConnecter);

        reponse.setVue(Boolean.FALSE);
        
        if(!question.getUtilisateur().getPseudo().equals(userConnecter.getPseudo())){
            reponse.setVue(Boolean.TRUE);
            questionManager.incrementeQuestionNotifer(questionId);
        }
        
        reponseManager.creer(reponse);
        
        Util.addFlashInfoMessage("Réponse " + reponse.getReponse()
                + " créé");
        
        return "/detail_question?idQuestion="+reponse.getQuestion().getQuestionId()+"faces-redirect=true";
    }

}
