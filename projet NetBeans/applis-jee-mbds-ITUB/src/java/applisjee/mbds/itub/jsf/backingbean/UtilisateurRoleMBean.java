/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.jsf.backingbean;

import applisjee.mbds.itub.ejb.entity.UtilisateurRole;
import applisjee.mbds.itub.ejb.session.UtilisateurRoleManager;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author Mirado
 */
@Named(value = "utilisateurRoleMBean")
@ViewScoped
public class UtilisateurRoleMBean implements Serializable{

    @EJB
    private UtilisateurRoleManager utilisateurRoleManager;
    
    private String pseudo;
    
    private UtilisateurRole utilisateurRole;

    /**
     *
     * @return
     */
    public List<UtilisateurRole> getAllUtilisateurRole() {
        return utilisateurRoleManager.getAllUtilisateurRole();
    }

    /**
     *
     * @return
     */
    public UtilisateurRole getUtilisateurRole() {
        return utilisateurRole;
    }

    /**
     *
     * @return
     */
    public String getPseudo() {
        return pseudo;
    }

    /**
     *
     * @param pseudo
     */
    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }
    
    /**
     * charger le role utilisateur
     */
    public void loadUtilisateurRole(){
        utilisateurRole = utilisateurRoleManager.findByPseudo(pseudo);
    }
    
    /**
     *modifier role utilisateur
     * @return
     */
    public String modifier(){
        utilisateurRoleManager.update(utilisateurRole);
        return "/admin/role/index?faces-redirect=true";
    }
}
