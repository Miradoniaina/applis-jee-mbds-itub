/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.jsf.backingbean;

import applisjee.mbds.itub.jsf.util.Util;
import applisjee.mbds.itub.session.services.UtilisateurServices;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Mirado
 */
@Named(value = "modificationMotDePasseMBean")
@RequestScoped
public class ModificationMotDePasseMBean {

    @Inject
    private ExternalContext externalContext;
    
    @Inject
    private FacesContext facesContext;
    
    @EJB
    private UtilisateurServices utilisateurServices;
    
    private String mdp;
    private String confMdp;

    /**
     *
     * @return
     */
    public String getMdp() {
        return mdp;
    }

    /**
     *
     * @param mdp
     */
    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    /**
     *
     * @return
     */
    public String getConfMdp() {
        return confMdp;
    }

    /**
     *
     * @param confMdp
     */
    public void setConfMdp(String confMdp) {
        this.confMdp = confMdp;
    }
    
    /**
     * Modification de mot de passe
     */
    public void modifierMdp(){
        HttpServletRequest request = (HttpServletRequest)externalContext.getRequest();
        String pseudo = request.getUserPrincipal().getName();
        
        try{
            utilisateurServices.modifierMotDePasse(pseudo, mdp, confMdp);
            Util.addFlashInfoMessage("Votre mot de passe a été modifié avec success");
        }catch (EJBException ejbex) {
            Util.addFlashErrorMessage(ejbex.getMessage());
        }
    }
}
