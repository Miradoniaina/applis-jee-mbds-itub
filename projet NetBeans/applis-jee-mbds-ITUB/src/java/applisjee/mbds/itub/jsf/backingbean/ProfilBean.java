/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.jsf.backingbean;

import applisjee.mbds.itub.ejb.entity.Utilisateur;
import applisjee.mbds.itub.ejb.entity.UtilisateurRole;
import applisjee.mbds.itub.ejb.session.UtilisateurManager;
import applisjee.mbds.itub.ejb.session.UtilisateurRoleManager;
import applisjee.mbds.itub.jsf.util.Util;
import applisjee.mbds.itub.session.services.UtilisateurServices;
import java.io.Serializable;
import java.util.Date;
import javax.ejb.EJB;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.security.enterprise.SecurityContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Andrianina_pc
 */
@Named(value = "profilBean")
@ViewScoped
public class ProfilBean implements Serializable {

    @Inject
    private ExternalContext externalContext;

    @EJB
    private UtilisateurManager utilisateurManager;

    @EJB
    private UtilisateurRoleManager utilisateurRoleManager;

    @EJB
    private UtilisateurServices utilisateurServices;

    private Utilisateur utilisateur = new Utilisateur();

    /**
     *
     * @return
     */
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    /**
     * récupère l'utilisateur connecter afin d'afficher les attributs dans les
     * inputs de la vue jsf
     */
    public void loadUser() {
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        String pseudo = request.getUserPrincipal().getName();
        Utilisateur userConnecter = utilisateurManager.Find(pseudo);
        utilisateur = userConnecter;
    }

    /**
     * Rempli les attributs de l'objet utilisateur récupère l'utilisateur
     * connecter pour l'attribut mot de passe de utilisateur
     *
     * save utilisateur dans la bdd
     * 
     *
     * sort le popup pour signifier a l'utilisateur que les modifs ont été
     * enregistrer
     *
     * reload la page de profil pour voir les modifications opérés
     *
     * @return
     */
    public String enregistrer() {
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        String pseudo = request.getUserPrincipal().getName();
        Utilisateur userConnecter = utilisateurManager.Find(pseudo);
        utilisateur.setMotDePasse(userConnecter.getMotDePasse());

        
        utilisateurServices.modifierUtilisateur(utilisateur);


        Util.addFlashInfoMessage("Utilisateur " + utilisateur.getPseudo()
                + " modifié");

        return "/connect/index?faces-redirect=true";
    }

}
