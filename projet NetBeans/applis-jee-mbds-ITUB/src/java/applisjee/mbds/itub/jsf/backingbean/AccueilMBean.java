/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.jsf.backingbean;

import applisjee.mbds.itub.ejb.entity.Question;
import applisjee.mbds.itub.ejb.session.QuestionManager;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author ADMIN
 */
@Named(value = "accueilMBean")
@SessionScoped
public class AccueilMBean implements Serializable {
    
    @EJB
    private QuestionManager questionManager;
    
    private String motcle;
    
    private String texte;
    
    private List<Question> questionList;

    /**
     * Creates a new instance of AccueilMBean
     */
    public AccueilMBean() {
    }

    public String getMotcle() {
        return motcle;
    }

    public void setMotcle(String motcle) {
        this.motcle = motcle;
    }

    public String getTexte() {
        return texte;
    }

    public void setTexte(String texte) {
        this.texte = texte;
    }

    public List<Question> getQuestionList() {
        if(questionList == null) setQuestionList(questionManager.search("", ""));
        return questionList;
    }

    public void setQuestionList(List<Question> questionList) {
        this.questionList = questionList;
    }
    
    /**
     * Fonction qui retourne le(s) résultat(s) de recherche pour les questions
     *
     * @return String
     * @param motcle
     * @param texte
     */
    public String getSearchQuestion(String motcle, String texte) {
        setQuestionList(questionManager.search(motcle, texte));
        System.out.println("applisjee.mbds.itub.jsf.backingbean.AccueilMBean.getSearchQuestion()="+questionList.size());
        return "/accueil?faces-redirect=true";
    }
    
}
