/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.config;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.annotation.FacesConfig;
import javax.security.enterprise.authentication.mechanism.http.CustomFormAuthenticationMechanismDefinition;
import javax.security.enterprise.authentication.mechanism.http.LoginToContinue;
import javax.security.enterprise.identitystore.DatabaseIdentityStoreDefinition;

/**
 * Configuration de l'application pour JSF 2.3 et pour la sécurité.
 * @author Mirado
 */
@ApplicationScoped
@FacesConfig
@DatabaseIdentityStoreDefinition(
  dataSourceLookup = "java:app/jdbc/applis-jee-mbds-ITUB",
  callerQuery = "select mot_de_passe from utilisateur where pseudo=?",
  groupsQuery = "select role from utilisateur_role where pseudo=?"
)
@CustomFormAuthenticationMechanismDefinition(
  loginToContinue = @LoginToContinue(
    loginPage = "/auth/login.xhtml",
    errorPage = ""
  )
)
public class Config {
    
}
