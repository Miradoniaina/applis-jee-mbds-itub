/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.ejb.session;

import applisjee.mbds.itub.ejb.entity.Utilisateur;
import applisjee.mbds.itub.ejb.entity.UtilisateurRole;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Mirado
 */
@Stateless
public class UtilisateurRoleManager {

    @PersistenceContext(unitName = "applis-jee-mbds-ITUBPU")
    private EntityManager em;

    /**
     *
     * @param ur
     */
    public void persist(UtilisateurRole ur) {
        em.persist(ur);
    }

    /**
     *
     * @param pseudo
     * @return
     */
    public UtilisateurRole findByPseudo(String pseudo) {
        return em.find(UtilisateurRole.class, pseudo);
    }
    
    /**
     *
     * @return
     */
    public List<UtilisateurRole> getAllUtilisateurRole() {
        Query query = em.createNamedQuery("UtilisateurRole.findAll");
        return query.getResultList();
    }
    
    /**
     *
     * @param ur
     * @return
     */
    public UtilisateurRole update(UtilisateurRole ur) {
        return em.merge(ur);
    }
    
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
