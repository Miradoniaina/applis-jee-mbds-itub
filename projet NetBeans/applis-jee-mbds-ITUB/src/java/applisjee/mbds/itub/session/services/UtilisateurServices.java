/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.session.services;

import applisjee.mbds.itub.ejb.entity.Utilisateur;
import applisjee.mbds.itub.ejb.entity.UtilisateurRole;
import applisjee.mbds.itub.ejb.session.UtilisateurManager;
import applisjee.mbds.itub.util.HashMdp;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Mirado
 */
@Stateless
public class UtilisateurServices {

    @EJB
    private UtilisateurManager um;

    @PersistenceContext(unitName = "applis-jee-mbds-ITUBPU")
    private EntityManager em;

    /**
     * Le "HashMdp" Pour coder le mot de passe
     *
     * @see HashMdp
     */
    @Inject
    private HashMdp passwordHash;

    /**
     *
     * @param u
     * @param mdpConf
     * @throws EJBException
     */
    public void inscription(Utilisateur u, String mdpConf) throws EJBException {

        if (!u.getMotDePasse().equals(mdpConf)) {
            throw new EJBException("Les mots de passes ne correspondent pas.");
        }

        u.setMotDePasse(passwordHash.generate(u.getMotDePasse()));

        UtilisateurRole ur = new UtilisateurRole(u.getPseudo());
        ur.setRole("utilisateur");

        u.setUtilisateurRole(ur);

        em.persist(u);
    }

    /**
     * Modification de l'utilisateur
     * @param u
     */
    public void modifierUtilisateur(Utilisateur u) {
        um.update(u);
    }
    
    /**
     * Modification du mot de passe de l'utilisateur
     * @param pseudo
     * @param mdp
     * @param confMdp
     * @throws EJBException
     */
    public void modifierMotDePasse(String pseudo, String mdp, String confMdp) throws EJBException {
        Utilisateur u = um.Find(pseudo);
        
        if (!mdp.equals(confMdp)) {
            throw new EJBException("Les mots de passes ne correspondent pas.");
        }
        
        u.setMotDePasse(passwordHash.generate(mdp));
        
        this.modifierUtilisateur(u);
    }
    
    
    /**
     *
     * @param pseudo
     * @return
     */
    public long nbPseudo(String pseudo) {
        Query q = em.createQuery("select count(u) from Utilisateur u where u.pseudo='" +pseudo+"'");
        return (long) q.getSingleResult();
    }
}
