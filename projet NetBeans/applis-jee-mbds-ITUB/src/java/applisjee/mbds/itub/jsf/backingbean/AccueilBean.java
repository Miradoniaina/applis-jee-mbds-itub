/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.jsf.backingbean;

import applisjee.mbds.itub.ejb.entity.Question;
import applisjee.mbds.itub.ejb.session.QuestionManager;
import applisjee.mbds.itub.jsf.util.Util;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.context.ExternalContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Andrianina_pc
 */
@Named(value = "accueilBean")
@ViewScoped
public class AccueilBean implements Serializable {

    @EJB
    private QuestionManager questionManager;

    @Inject
    private ExternalContext externalContext;

    private List<Question> questionList;

    private String pseudoUtilisateur = " ";

    /**
     *
     * @return
     */
    public List<Question> getQuestions() {
        
        if (questionList == null) {
            questionList = questionManager.findAll();
        }
        return questionList;
    }

    /**
     *
     * @param question
     */
    public void resolu(Question question) {
        questionManager.marquerResolu(question);
        Util.addFlashInfoMessage("Marquer comme résolu");
    }

    /**
     *
     * @return
     */
    public String getPseudoUtilisateur() {
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        if (request.getUserPrincipal() != null) {
            String pseudo = request.getUserPrincipal().getName();
            pseudoUtilisateur = pseudo;
        }
        return pseudoUtilisateur;
    }

}
