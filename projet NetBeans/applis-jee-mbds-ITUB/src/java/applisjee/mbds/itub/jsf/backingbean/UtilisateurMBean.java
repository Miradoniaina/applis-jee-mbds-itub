/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.jsf.backingbean;

import applisjee.mbds.itub.ejb.entity.Utilisateur;
import applisjee.mbds.itub.ejb.session.UtilisateurManager;
import applisjee.mbds.itub.jsf.util.Util;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Mirado
 */
@Named(value = "utilisateurMBean")
@RequestScoped
public class UtilisateurMBean {
    
    private List<Utilisateur> utilisateurList;
    
    @EJB
    private UtilisateurManager utilisateurManager;

    /**
     *
     * @return
     */
    public List<Utilisateur> getAllUtilisateur() {
        return utilisateurManager.findAll();
    }
    
    /**
     *
     * @param u
     * @return
     */
    public String supprimerCompte(Utilisateur u){
        utilisateurManager.supprimerCompte(u);
        Util.addFlashInfoMessage("Compte de " + u.getPseudo()+ " supprimé");
        return "/admin/utilisateur/index?faces-redirect=true";
    }
}
