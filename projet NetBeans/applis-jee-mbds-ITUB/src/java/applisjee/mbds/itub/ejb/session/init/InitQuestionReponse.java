/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.ejb.session.init;

import applisjee.mbds.itub.ejb.entity.Question;
import applisjee.mbds.itub.ejb.entity.Reponse;
import applisjee.mbds.itub.ejb.entity.Utilisateur;
import applisjee.mbds.itub.ejb.session.QuestionManager;
import applisjee.mbds.itub.ejb.session.ReponseManager;
import applisjee.mbds.itub.ejb.session.UtilisateurManager;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author Mirado
 */
@Singleton
@LocalBean
@DependsOn({"InitMotCle"})
@Startup
public class InitQuestionReponse {

    @EJB
    private UtilisateurManager utilisateurManager;

    @EJB
    private QuestionManager questionManager;

    @EJB
    private ReponseManager reponseManager;
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    @PostConstruct
    private void initQuestion() {

        Question q1 = null, q2 = null;
        Utilisateur u = utilisateurManager.Find("miradoniaina");
        Utilisateur u2 = utilisateurManager.Find("layton");
        Utilisateur u3 = utilisateurManager.Find("mbola");

        if (questionManager.nbQuestion() == 0) {
            
            

            q1 = new Question(
                    "What is PrivatePageCount in Win32_Process?",
                    "PrivatePageCount documentation says: PrivatePageCount Data type: uint64 Qualifiers: MappingStrings (\"Win32API|Process Status|SYSTEM_PROCESS_INFORMATION|PrivatePageCount\"), DisplayName (\"...",
                    new Date(),
                    3,
                    "windows;c#",
                    u
            );

            q2 = new Question(
                    "C++ Bluetooth Programming with Rasberrry Pi",
                    "I am trying to learn how to send information from my Windows PC to a Linux Raspberry Pi. I have been just trying to understand some of the C++ Bluetooth libraries. And I keep getting an error when I ...",
                    new Date(),
                    2,
                    "c++;linux;windows;bluetooth;raspberry-pi",
                    u
            );

            Question q3 = new Question(
                    "'pip3' is not recognized as an internal or external command, operable program or batch file",
                    "(base) F:>pip3 install nltk 'pip3' is not recognized as an internal or external command, operable program or batch file. what should i do to resolve this issue. Thanks in advance",
                    new Date(),
                    0,
                    "python;windows;pip",
                    u2
            );
            
            Question q4 = new Question(
                    "How to use PathCreateFromUrlA API",
                    "I need to convert a uri path like file:///C:/test folder/file.text to windows style path C:/test folder/file.text. For that, I am trying to use PathCreatesFromUrlA API using the following code but I ...",
                    new Date(),
                    0,
                    "c++;windows",
                    u2
            );

            Question q5 = new Question(
                    "What are the keycodes `getwch` returns?",
                    "I'm using msvcrt.getwch to make an interactive prompt in Python targeting Windows. The documentation says: Read a keypress and return the resulting character as a byte string. Nothing is echoed to ...",
                    new Date(),
                    0,
                    "windows 10;linux",
                    u3
            );

            Question q6 = new Question(
                    "Quicksort vs. In-place Merge Sort",
                    "<div class=\"postcell post-layout--right\">\n"
                    + "\n"
                    + "    <div class=\"post-text\" itemprop=\"text\">\n"
                    + "\n"
                    + "        <p>I was researching whether or not Quicksort is better than Merge Sort, and most sources were in consensus that\n"
                    + "            Quicksort is better, because it is in-place, while Merge Sort is not. However, there exist in-place Merge\n"
                    + "            Sort algorithms, which invalidate the \"it needs extra space\" argument. So which is better, Quicksort or\n"
                    + "            In-place Merge Sort?</p>\n"
                    + "\n"
                    + "        <p>PS: When I say better, I mean faster, since space is not an issue for either of these sorts.</p>\n"
                    + "\n"
                    + "        <p>EDIT: Is any speed lost when switching from out-of-place Merge Sort to in-place Merge Sort?</p>\n"
                    + "    </div>\n"
                    + "\n"
                    + "</div>",
                    new Date(),
                    0,
                    "sorting;quicksort",
                    u3
            );

            Question q7 = new Question(
                    "Tornado decorator cors headers",
                    "<div class=\"postcell post-layout--right\">\n"
                    + "\n"
                    + "    <div class=\"post-text\" itemprop=\"text\">\n"
                    + "\n"
                    + "        <p>I have my ionic as frontend, which append a Bearer Token in each Request. My Tornado server is already\n"
                    + "            configured for CORS. The problem is when I activate my decorator to check if that request has\n"
                    + "            'Authorization' header, I don't why, that header disappeared. There's no 'Authorization' header. But If I\n"
                    + "            disable this header, everything is ok and tornado show me that header. </p>\n"
                    + "\n"
                    + "        <p>Any idea?</p>\n"
                    + "\n"
                    + "        <p>I override RequestHandler set_default_headers method</p>\n"
                    + "\n"
                    + "        <p>With these headers:</p>\n"
                    + "\n"
                    + "        <pre><code> set_default_headers(self):\n"
                    + "            self.set_header(\"Access-Control-Allow-Origin\", \"*\")\n"
                    + "            self.set_header(\"Access-Control-Allow-Headers\", \"authorization\")\n"
                    + "            self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')\n"
                    + "    </code></pre>\n"
                    + "\n"
                    + "        <p>Decorator</p>\n"
                    + "\n"
                    + "        <pre><code>class EvaluationHandler(RequestHandler)\n"
                    + "    \n"
                    + "    def get(self):\n"
                    + "     print(handler.request.headers) &lt;--- Good headers\n"
                    + "    </code></pre>\n"
                    + "\n"
                    + "        <p>My token decorator to check it:</p>\n"
                    + "\n"
                    + "        <pre><code>def jwtauth(handler_class):\n"
                    + "        def wrap_execute(handler_execute):\n"
                    + "            def require_auth(handler, kwargs):\n"
                    + "                auth = handler.request.headers.get('Authorization', None)\n"
                    + "                print(handler.request.headers) &lt;---- Bad Headers\n"
                    + "                print(auth)\n"
                    + "                if auth:\n"
                    + "                    if not loginRepository.validToken(auth):\n"
                    + "                        return_header_error(handler)\n"
                    + "                else:\n"
                    + "                    return_header_error(handler)\n"
                    + "                    handler._transforms = []\n"
                    + "                    handler.write(MISSING_AUTHORIZATION_KEY)\n"
                    + "                    handler.finish()\n"
                    + "    \n"
                    + "                return True\n"
                    + "    \n"
                    + "            def _execute(self, transforms, *args, **kwargs):\n"
                    + "                try:\n"
                    + "                    require_auth(self, kwargs)\n"
                    + "                except Exception:\n"
                    + "                    return False\n"
                    + "    \n"
                    + "                return handler_execute(self, transforms, *args, **kwargs)\n"
                    + "    \n"
                    + "            return _execute\n"
                    + "    \n"
                    + "        handler_class._execute = wrap_execute(handler_class._execute)\n"
                    + "        return handler_class\n"
                    + "    </code></pre>\n"
                    + "    </div>\n"
                    + "</div>",
                    new Date(),
                    0,
                    "windows;docker",
                    u
            );
            
            
            questionManager.creer(q1);
            questionManager.creer(q2);
            questionManager.creer(q3);
            questionManager.creer(q4);
            questionManager.creer(q5);
            questionManager.creer(q6);
            questionManager.creer(q7);
        }

        if (reponseManager.nbReponse() == 0) {
            Reponse r1 = new Reponse(
                    "<div class=\"post-text\" itemprop=\"text\">\n"
                    + "\n"
                    + "    <p><code>PrivatePageCount</code> <a\n"
                    + "            href=\"https://docs.microsoft.com/en-us/windows/desktop/CIMWin32Prov/win32-process\"\n"
                    + "            rel=\"nofollow noreferrer\">documentation</a> says:</p>\n"
                    + "\n"
                    + "    <blockquote>\n"
                    + "        <p><code>PrivatePageCount</code></p>\n"
                    + "\n"
                    + "        <p>Data type: uint64</p>\n"
                    + "\n"
                    + "        <p>Qualifiers: MappingStrings (\"Win32API|Process Status|SYSTEM_PROCESS_INFORMATION|PrivatePageCount\"),\n"
                    + "            DisplayName (\"Private Page Count\")</p>\n"
                    + "\n"
                    + "        <p>Current <strong>number of pages</strong> allocated that are only accessible to the process represented by\n"
                    + "            this Win32_Process instance.</p>\n"
                    + "    </blockquote>\n"
                    + "\n"
                    + "    <p>But inspecting the ManagementObjectSearcher values, it looks like the same value as <code>PageFileUsage</code>,\n"
                    + "        but in bytes instead of kilobytes.</p>\n"
                    + "\n"
                    + "    <pre><code>using (var items = new ManagementObjectSearcher(String.Format(\"Select * From Win32_Process\")).Get())\n"
                    + "{\n"
                    + "    foreach (var item in items)\n"
                    + "    {\n"
                    + "        var PageFileUsage = (UInt32)item[\"PageFileUsage\"]; // kb\n"
                    + "        var PrivatePageCount = (UInt64)item[\"PrivatePageCount\"]; // same as PageFileUsage?\n"
                    + "\n"
                    + "        Debug.Assert(PageFileUsage == (PrivatePageCount / 1024));\n"
                    + "    }\n"
                    + "}\n"
                    + "</code></pre>\n"
                    + "</div>",
                    new Date(2019 - 1900, 2 - 1, 22),
                    0,
                    false,
                    u2,
                    q1
            );
            Reponse r2 = new Reponse(
                    "<blockquote>\n"
                    + "  <p><code>PrivatePageCount</code></p>\n"
                    + "  \n"
                    + "  <p>Data type: uint64</p>\n"
                    + "  \n"
                    + "  <p>Qualifiers: MappingStrings (\"Win32API|Process Status|SYSTEM_PROCESS_INFORMATION|PrivatePageCount\"), DisplayName (\"Private Page Count\")</p>\n"
                    + "  \n"
                    + "  <p>Current <strong>number of pages</strong> allocated that are only accessible to the process represented by this Win32_Process instance.</p>\n"
                    + "</blockquote>",
                    new Date(2019 - 1900, 3 - 1, 10),
                    0,
                    false,
                    u,
                    q1
            );
            Reponse r3 = new Reponse(
                    "<p>Multiplying this value by the system's page size (<code>Environment.SystemPageSize</code>), gives a value in\n"
                    + "    terabytes, but it is not close to Virtual Memory (address space).</p>\n"
                    + "\n"
                    + "<p>Could the documentation be wrong and <code>PrivatePageCount</code> be equivalent to <code>PageFileUsage</code>?</p>\n"
                    + "</div>\n"
                    + "\n"
                    + "<div class=\"post-taglist grid gs4 gsy fd-column\">\n"
                    + "    <div class=\"grid ps-relative d-block\">\n"
                    + "        <a href=\"/questions/tagged/c%23\" class=\"post-tag\" title=\"show questions tagged &#39;c#&#39;\" rel=\"tag\">c#</a> <a\n"
                    + "            href=\"/questions/tagged/windows\" class=\"post-tag\" title=\"show questions tagged &#39;windows&#39;\"\n"
                    + "            rel=\"tag\">windows</a>\n"
                    + "    </div>\n"
                    + "</div>\n"
                    + "\n"
                    + "<div class=\"mb0 \">\n"
                    + "    <div class=\"mt16 pt4 grid gs8 gsy fw-wrap jc-end ai-start\">\n"
                    + "        <div class=\"grid--cell mr16\" style=\"flex: 1 1 100px;\">\n"
                    + "            <div class=\"post-menu\"><a href=\"/q/55878282\" title=\"short permalink to this question\" class=\"short-link\"\n"
                    + "                    itemprop=\"url\" id=\"link-post-55878282\">share</a><span class=\"lsep\">|</span><a\n"
                    + "                    href=\"/posts/55878282/edit\" class=\"suggest-edit-post\" title=\"\">improve this question</a></div>\n"
                    + "        </div>\n"
                    + "\n"
                    + "        <div class=\"post-signature owner grid--cell\">\n"
                    + "            <div class=\"user-info user-hover\">\n"
                    + "                <div class=\"user-action-time\">\n"
                    + "                    asked <span title=\"2019-04-27 07:25:18Z\" class=\"relativetime\">3 hours ago</span>\n"
                    + "                </div>\n"
                    + "                <div class=\"user-gravatar32\">\n"
                    + "                    <a href=\"/users/27211/kiewic\">\n"
                    + "                        <div class=\"gravatar-wrapper-32\"><img\n"
                    + "                                src=\"https://www.gravatar.com/avatar/ae536c1c0bc292aaca19574b58587ff1?s=32&amp;d=identicon&amp;r=PG\"\n"
                    + "                                alt=\"\" width=\"32\" height=\"32\"></div>\n"
                    + "                    </a>\n"
                    + "                </div>\n"
                    + "                <div class=\"user-details\" itemprop=\"author\" itemscope itemtype=\"http://schema.org/Person\">\n"
                    + "                    <a href=\"/users/27211/kiewic\">kiewic</a><span class=\"d-none\" itemprop=\"name\">kiewic</span>\n"
                    + "                    <div class=\"-flair\">\n"
                    + "                        <span class=\"reputation-score\" title=\"reputation score 10,464\" dir=\"ltr\">10.5k</span><span\n"
                    + "                            title=\"9 gold badges\"><span class=\"badge1\"></span><span\n"
                    + "                                class=\"badgecount\">9</span></span><span title=\"61 silver badges\"><span\n"
                    + "                                class=\"badge2\"></span><span class=\"badgecount\">61</span></span><span\n"
                    + "                            title=\"76 bronze badges\"><span class=\"badge3\"></span><span\n"
                    + "                                class=\"badgecount\">76</span></span>\n"
                    + "                    </div>\n"
                    + "                </div>\n"
                    + "            </div>\n"
                    + "\n"
                    + "        </div>\n"
                    + "    </div>\n"
                    + "</div>",
                    new Date(),
                    0,
                    false,
                    u2,
                    q1
            );
            Reponse r4 = new Reponse(
                    "<pre><code>#include &lt;iostream&gt;\n"
                    + "#include &lt;windows.system.power.h&gt;\n"
                    + "//#include &lt;IOCTL_BATTERY_QUERY_STATUS&gt;\n"
                    + "//#include &lt;Bthsdpdef.h&gt;\n"
                    + "//#include &lt;bluetoothapis.h&gt;\n"
                    + "#include &lt;windows.devices.bluetooth.h&gt;\n"
                    + "#include &lt;bluetoothapis.h&gt;\n"
                    + "\n"
                    + "int main()\n"
                    + "{\n"
                    + "    std::cout &lt;&lt; \"Hello World!\\n\"; \n"
                    + "\n"
                    + "    SYSTEM_POWER_STATUS Test;\n"
                    + "    GetSystemPowerStatus(&amp;Test);\n"
                    + "\n"
                    + "    HANDLE hRadio = NULL;\n"
                    + "    //BOOL t1 = BluetoothIsDiscoverable(hRadio);\n"
                    + "\n"
                    + "    BOOL fEnabled = true;\n"
                    + "\n"
                    + "    BOOL t2 = BluetoothEnableIncomingConnections(hRadio, fEnabled);\n"
                    + "    //BOOL t3 = BluetoothEnableDiscovery(hRadio, fEnabled);\n"
                    + "\n"
                    + "\n"
                    + "    //SOCKADDR BT;\n"
                    + "\n"
                    + "\n"
                    + "    system(\"pause\");\n"
                    + "}\n"
                    + "</code></pre>",
                    new Date(2019 - 1900, 4 - 1, 9),
                    0,
                    false,
                    u2,
                    q2
            );

            Reponse r5 = new Reponse(
                    "<div class=\"post-layout--right\">\n"
                    + "    <div id=\"comments-55877205\" class=\"comments js-comments-container \" data-post-id=\"55877205\">\n"
                    + "        <ul class=\"comments-list js-comments-list\" data-remaining-comments-count=\"1\" data-canpost=\"false\"\n"
                    + "            data-cansee=\"true\" data-comments-unavailable=\"false\" data-addlink-disabled=\"true\">\n"
                    + "\n"
                    + "\n"
                    + "            <li id=\"comment-98413888\" class=\"comment js-comment \" data-comment-id=\"98413888\">\n"
                    + "                <div class=\"js-comment-actions comment-actions\">\n"
                    + "                    <div class=\"comment-score js-comment-edit-hide\">\n"
                    + "                    </div>\n"
                    + "                </div>\n"
                    + "                <div class=\"comment-text js-comment-text-and-form\">\n"
                    + "                    <div class=\"comment-body js-comment-edit-hide\">\n"
                    + "\n"
                    + "                        <span class=\"comment-copy\">Have you linked against bthprops?</span>\n"
                    + "\n"
                    + "                        &ndash;&nbsp;<a href=\"/users/6556709/user6556709\" title=\"463 reputation\"\n"
                    + "                            class=\"comment-user\">user6556709</a>\n"
                    + "                        <span class=\"comment-date\" dir=\"ltr\"><span title=\"2019-04-27 04:21:26Z\"\n"
                    + "                                class=\"relativetime-clean\">7 hours ago</span></span>\n"
                    + "                    </div>\n"
                    + "                </div>\n"
                    + "            </li>\n"
                    + "            <li id=\"comment-98413904\" class=\"comment js-comment \" data-comment-id=\"98413904\">\n"
                    + "                <div class=\"js-comment-actions comment-actions\">\n"
                    + "                    <div class=\"comment-score js-comment-edit-hide\">\n"
                    + "                    </div>\n"
                    + "                </div>\n"
                    + "                <div class=\"comment-text js-comment-text-and-form\">\n"
                    + "                    <div class=\"comment-body js-comment-edit-hide\">\n"
                    + "\n"
                    + "                        <span class=\"comment-copy\">@user6556709 I&#39;m not sure what that means</span>\n"
                    + "\n"
                    + "                        &ndash;&nbsp;<a href=\"/users/5508364/calcguy\" title=\"39 reputation\"\n"
                    + "                            class=\"comment-user owner\">CalcGuy</a>\n"
                    + "                        <span class=\"comment-date\" dir=\"ltr\"><span title=\"2019-04-27 04:24:47Z\"\n"
                    + "                                class=\"relativetime-clean\">7 hours ago</span></span>\n"
                    + "                    </div>\n"
                    + "                </div>\n"
                    + "            </li>\n"
                    + "            <li id=\"comment-98413943\" class=\"comment js-comment \" data-comment-id=\"98413943\">\n"
                    + "                <div class=\"js-comment-actions comment-actions\">\n"
                    + "                    <div class=\"comment-score js-comment-edit-hide\">\n"
                    + "                    </div>\n"
                    + "                </div>\n"
                    + "                <div class=\"comment-text js-comment-text-and-form\">\n"
                    + "                    <div class=\"comment-body js-comment-edit-hide\">\n"
                    + "\n"
                    + "                        <span class=\"comment-copy\">The message says it can&#39;t find an external reference. That\n"
                    + "                            usually means that you are missing an external library (static or dynamic). The Bthprops.lib\n"
                    + "                            is the library which contains the Bluetooth stuff. So you need to add Bthprops.lib to your\n"
                    + "                            linker dependencies.</span>\n"
                    + "\n"
                    + "                        &ndash;&nbsp;<a href=\"/users/6556709/user6556709\" title=\"463 reputation\"\n"
                    + "                            class=\"comment-user\">user6556709</a>\n"
                    + "                        <span class=\"comment-date\" dir=\"ltr\"><span title=\"2019-04-27 04:30:48Z\"\n"
                    + "                                class=\"relativetime-clean\">6 hours ago</span></span>\n"
                    + "                        <span title=\"this comment was edited 2 times\">\n"
                    + "                            <svg aria-hidden=\"true\" class=\"svg-icon va-text-bottom o50 iconPencilSm\" width=\"14\"\n"
                    + "                                height=\"14\" viewBox=\"0 0 14 14\">\n"
                    + "                                <path\n"
                    + "                                    d=\"M8.37 3.7L2 10.11V12h1.88l6.37-6.43zM12.23 2.83L11.1 1.71a.5.5 0 0 0-.7 0L9.2 2.86l1.88 1.84 1.14-1.16a.5.5 0 0 0 0-.71z\" />\n"
                    + "                                </svg>\n"
                    + "                        </span>\n"
                    + "                    </div>\n"
                    + "                </div>\n"
                    + "            </li>\n"
                    + "            <li id=\"comment-98414191\" class=\"comment js-comment \" data-comment-id=\"98414191\">\n"
                    + "                <div class=\"js-comment-actions comment-actions\">\n"
                    + "                    <div class=\"comment-score js-comment-edit-hide\">\n"
                    + "                    </div>\n"
                    + "                </div>\n"
                    + "                <div class=\"comment-text js-comment-text-and-form\">\n"
                    + "                    <div class=\"comment-body js-comment-edit-hide\">\n"
                    + "\n"
                    + "                        <span class=\"comment-copy\">@user6556709 how do I go about doing that</span>\n"
                    + "\n"
                    + "                        &ndash;&nbsp;<a href=\"/users/5508364/calcguy\" title=\"39 reputation\"\n"
                    + "                            class=\"comment-user owner\">CalcGuy</a>\n"
                    + "                        <span class=\"comment-date\" dir=\"ltr\"><span title=\"2019-04-27 05:00:33Z\"\n"
                    + "                                class=\"relativetime-clean\">6 hours ago</span></span>\n"
                    + "                    </div>\n"
                    + "                </div>\n"
                    + "            </li>\n"
                    + "            <li id=\"comment-98414227\" class=\"comment js-comment \" data-comment-id=\"98414227\">\n"
                    + "                <div class=\"js-comment-actions comment-actions\">\n"
                    + "                    <div class=\"comment-score js-comment-edit-hide\">\n"
                    + "                    </div>\n"
                    + "                </div>\n"
                    + "                <div class=\"comment-text js-comment-text-and-form\">\n"
                    + "                    <div class=\"comment-body js-comment-edit-hide\">\n"
                    + "\n"
                    + "                        <span class=\"comment-copy\">You linker looks like VS, so: <a\n"
                    + "                                href=\"https://docs.microsoft.com/en-us/cpp/build/reference/dot-lib-files-as-linker-input?view=vs-2019\"\n"
                    + "                                rel=\"nofollow noreferrer\">docs.microsoft.com/en-us/cpp/build/reference/&hellip;</a> and\n"
                    + "                            the Bthprops.lib is somewhere in the Windows SDK.</span>\n"
                    + "\n"
                    + "                        &ndash;&nbsp;<a href=\"/users/6556709/user6556709\" title=\"463 reputation\"\n"
                    + "                            class=\"comment-user\">user6556709</a>\n"
                    + "                        <span class=\"comment-date\" dir=\"ltr\"><span title=\"2019-04-27 05:05:22Z\"\n"
                    + "                                class=\"relativetime-clean\">6 hours ago</span></span>\n"
                    + "                        <span title=\"this comment was edited 1 time\">\n"
                    + "                            <svg aria-hidden=\"true\" class=\"svg-icon va-text-bottom o50 iconPencilSm\" width=\"14\"\n"
                    + "                                height=\"14\" viewBox=\"0 0 14 14\">\n"
                    + "                                <path\n"
                    + "                                    d=\"M8.37 3.7L2 10.11V12h1.88l6.37-6.43zM12.23 2.83L11.1 1.71a.5.5 0 0 0-.7 0L9.2 2.86l1.88 1.84 1.14-1.16a.5.5 0 0 0 0-.71z\" />\n"
                    + "                                </svg>\n"
                    + "                        </span>\n"
                    + "                    </div>\n"
                    + "                </div>\n"
                    + "            </li>\n"
                    + "        </ul>\n"
                    + "    </div>\n"
                    +"</div>",
                    new Date(),
                    0,
                    false,
                    u3,
                    q2
            );

            reponseManager.creer(r1);
            reponseManager.creer(r2);
            reponseManager.creer(r3);
            reponseManager.creer(r4);
            reponseManager.creer(r5);
        }
    }

}
