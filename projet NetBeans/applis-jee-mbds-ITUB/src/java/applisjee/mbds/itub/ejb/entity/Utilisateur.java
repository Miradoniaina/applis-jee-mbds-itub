/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.ejb.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mirado
 */
@Entity
@Table(name = "UTILISATEUR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Utilisateur.findAll", query = "SELECT u FROM Utilisateur u"),
    @NamedQuery(name = "Utilisateur.findByPseudo", query = "SELECT u FROM Utilisateur u WHERE u.pseudo = :pseudo"),
    @NamedQuery(name = "Utilisateur.findByEmail", query = "SELECT u FROM Utilisateur u WHERE u.email = :email"),
    @NamedQuery(name = "Utilisateur.findByNom", query = "SELECT u FROM Utilisateur u WHERE u.nom = :nom"),
    @NamedQuery(name = "Utilisateur.findByPrenom", query = "SELECT u FROM Utilisateur u WHERE u.prenom = :prenom"),
    @NamedQuery(name = "Utilisateur.findByDateDeNaissance", query = "SELECT u FROM Utilisateur u WHERE u.dateDeNaissance = :dateDeNaissance"),
    @NamedQuery(name = "Utilisateur.findByMotDePasse", query = "SELECT u FROM Utilisateur u WHERE u.motDePasse = :motDePasse")})
public class Utilisateur implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PSEUDO")
    private String pseudo;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "NOM")
    private String nom;
    @Column(name = "PRENOM")
    private String prenom;
    @Column(name = "DATE_DE_NAISSANCE")
    @Temporal(TemporalType.DATE)
    private Date dateDeNaissance;
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "PHOTO")
    private Serializable photo;
    @Column(name = "MOT_DE_PASSE")
    private String motDePasse;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "utilisateur")
    private UtilisateurRole utilisateurRole;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "utilisateur")
    private Collection<Question> questionCollection;
    
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "utilisateur")
//    private Collection<Question> questionCollection;
    
    /**
     * constructeur
     */
    public Utilisateur() {
    }

    /**
     *
     * @param pseudo
     */
    public Utilisateur(String pseudo) {
        this.pseudo = pseudo;
    }

    /**
     *
     * @return
     */
    public String getPseudo() {
        return pseudo;
    }

    /**
     *
     * @param pseudo
     */
    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    /**
     *
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     *
     * @param prenom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     *
     * @return
     */
    public Date getDateDeNaissance() {
        return dateDeNaissance;
    }

    /**
     *
     * @param dateDeNaissance
     */
    public void setDateDeNaissance(Date dateDeNaissance) {
        this.dateDeNaissance = dateDeNaissance;
    }

    /**
     *
     * @return
     */
    public Serializable getPhoto() {
        return photo;
    }
    
    /**
     *
     * @param photo
     */
    public void setPhoto(Serializable photo) {
        this.photo = photo;
    }

    /**
     *
     * @return
     */
    public String getMotDePasse() {
        return motDePasse;
    }

    /**
     *
     * @param motDePasse
     */
    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    /**
     *
     * @return
     */
    public UtilisateurRole getUtilisateurRole() {
        return utilisateurRole;
    }

    /**
     *
     * @param utilisateurRole
     */
    public void setUtilisateurRole(UtilisateurRole utilisateurRole) {
        this.utilisateurRole = utilisateurRole;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pseudo != null ? pseudo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Utilisateur)) {
            return false;
        }
        Utilisateur other = (Utilisateur) object;
        if ((this.pseudo == null && other.pseudo != null) || (this.pseudo != null && !this.pseudo.equals(other.pseudo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "applisjee.mbds.itub.ejb.entity.Utilisateur[ pseudo=" + pseudo + " ]";
    }

}
