/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.ejb.session;

import applisjee.mbds.itub.ejb.entity.AVoter;
import applisjee.mbds.itub.ejb.entity.Question;
import applisjee.mbds.itub.ejb.entity.Reponse;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Andrianina_pc
 */
@Stateless
public class ReponseManager {

    @PersistenceContext(unitName = "applis-jee-mbds-ITUBPU")
    private EntityManager em;
    
    @EJB
    private QuestionManager questionManager;

    /**
     *
     * @param reponse
     */
    public void creer(Reponse reponse) {
        em.persist(reponse);
    }
    
    /**
     * 
     * @param reponse 
     */
    public void update(Reponse reponse){
        em.merge(reponse);
    }
    

    /**
     *
     * @return
     */
    public long nbReponse() {
        Query q = em.createQuery("select count(q) from Reponse q");
        return (long) q.getSingleResult();
    }

    /**
     *
     * @return
     */
    public List<Reponse> findAll() {
        Query q = em.createNamedQuery("Reponse.findAll");
        return q.getResultList();
    }

    /**
     *
     * @param idReponse
     * @return
     */
    public Reponse findById(long idReponse) {
        return em.find(Reponse.class, idReponse);
    }

    /**
     * Retourne une liste des réponses qui pour la question de'id questionId
     *
     * @param questionId
     * @return 
     */
    public List<Reponse> findByIdQuestion(Long questionId) {
        Query q = em.createNamedQuery("Reponse.findByIdQuestion").setParameter("questionId", questionId);
        return q.getResultList();
    }

    /**
     * Mettre a jour une si il a déjà été vue
     *
     * @param questionId
     */
    public void miseAJourReponsesVue(Long questionId) {
        List<Reponse> reponses = this.findByIdQuestion(questionId);
        
        for (Reponse reponse : reponses) {
            reponse.setVue(Boolean.TRUE);
            em.merge(reponse);
        }
        
        Question question = reponses.get(0).getQuestion();
        
        question.setNotifier(0);
    }
    
    /**
     *
     * @param idReponse
     * @param pseudo
     * @return
     */
    public boolean peutVoter(Long idReponse, String pseudo){
        Query q = em.createQuery("select count(a) from AVoter a where a.idReponse = " + idReponse + " AND a.pseudo = '"+pseudo+"'");
        long resultat = (long) q.getSingleResult();
        if(resultat == 0) return true;
        return false;
    }
    
    /**
     *
     * @param idReponse
     * @param pseudo
     */
    public void aVoter(Long idReponse, String pseudo){
        AVoter avoter = new AVoter();
        avoter.setIdReponse(idReponse);
        avoter.setPseudo(pseudo);
        em.persist(avoter);
    }
}
