/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.ejb.session;

import applisjee.mbds.itub.ejb.entity.MotCle;
import applisjee.mbds.itub.ejb.entity.Question;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author ADMIN
 */
@Stateless
public class MotCleManager {

    @PersistenceContext(unitName = "applis-jee-mbds-ITUBPU")
    private EntityManager em;
    
    /**
     *
     * @param motCle
     */
    public void creer(MotCle motCle) {
        String mot = motCle.getMot().toLowerCase();
        Query q = em.createQuery("select count(m) from MotCle m where lower(m.mot) = '" + mot + "'");
        long resultat = (long) q.getSingleResult();
        if(resultat != 1) em.persist(motCle);
    }
    
    /**
     *
     * @return
     */
    public long nbMotCle() {
        Query q = em.createQuery("select count(m) from MotCle m");
        return (long) q.getSingleResult();
    }
    
    /**
     *
     * @return
     */
    public List<Question> findAll() {
        Query q = em.createNamedQuery("MotCle.findAll");
        return q.getResultList();
    }
    
    /**
     * 
     * @param idMotCle
     * @return 
     */
    public MotCle findById(long idMotCle) {
        return em.find(MotCle.class, idMotCle);
    }
}
