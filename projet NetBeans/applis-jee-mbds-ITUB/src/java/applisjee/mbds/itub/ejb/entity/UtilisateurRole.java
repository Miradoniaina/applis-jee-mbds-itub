/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.ejb.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mirado
 */
@Entity
@Table(name = "UTILISATEUR_ROLE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UtilisateurRole.findAll", query = "SELECT u FROM UtilisateurRole u"),
    @NamedQuery(name = "UtilisateurRole.findByPseudo", query = "SELECT u FROM UtilisateurRole u WHERE u.pseudo = :pseudo"),
    @NamedQuery(name = "UtilisateurRole.findByRole", query = "SELECT u FROM UtilisateurRole u WHERE u.role = :role")})
public class UtilisateurRole implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PSEUDO")
    private String pseudo;
    @Column(name = "ROLE")
    private String role;
    @JoinColumn(name = "PSEUDO", referencedColumnName = "PSEUDO", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Utilisateur utilisateur;

    /**
     * constructeur
     */
    public UtilisateurRole() {
    }

    /**
     *
     * @param pseudo
     */
    public UtilisateurRole(String pseudo) {
        this.pseudo = pseudo;
    }
    
    /**
     *
     * @param pseudo
     * @param role
     */
    public UtilisateurRole(String pseudo, String role) {
        this.pseudo = pseudo;
        this.role = role;
    }

    /**
     *
     * @return
     */
    public String getPseudo() {
        return pseudo;
    }

    /**
     *
     * @param pseudo
     */
    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    /**
     *
     * @return
     */
    public String getRole() {
        return role;
    }

    /**
     *
     * @param role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     *
     * @return
     */
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    /**
     *
     * @param utilisateur
     */
    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pseudo != null ? pseudo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UtilisateurRole)) {
            return false;
        }
        UtilisateurRole other = (UtilisateurRole) object;
        if ((this.pseudo == null && other.pseudo != null) || (this.pseudo != null && !this.pseudo.equals(other.pseudo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "applisjee.mbds.itub.ejb.entity.UtilisateurRole[ pseudo=" + pseudo + " ]";
    }
    
}
