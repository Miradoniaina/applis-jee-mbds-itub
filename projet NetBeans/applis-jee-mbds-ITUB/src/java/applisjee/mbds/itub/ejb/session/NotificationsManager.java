/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.ejb.session;

import applisjee.mbds.itub.ejb.entity.Question;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Mirado
 */
@Stateless
public class NotificationsManager {

    @PersistenceContext(unitName = "applis-jee-mbds-ITUBPU")
    private EntityManager em;
    

    /**
     *
     * @param pseudo
     * @return
     */
    public Long notificationTotal(String pseudo){
        Query q = em.createQuery("select sum(q.notifier) from Question q where q.utilisateur.pseudo = '" +pseudo+"'");
        return (long) q.getSingleResult();
    }
    
    /**
     *
     * @param pseudo
     * @return
     */
    public List<Question> questionNotifie(String pseudo){
        Query q = em.createQuery("select q from Question q where q.utilisateur.pseudo = '" +pseudo+"' and q.notifier != 0");
        return q.getResultList();
    }
}
