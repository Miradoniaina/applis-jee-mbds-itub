/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.ejb.session.init;

import applisjee.mbds.itub.ejb.entity.MotCle;
import applisjee.mbds.itub.ejb.session.MotCleManager;
import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author ADMIN
 */
@Singleton
@LocalBean
@DependsOn({"Init"})
@Startup
public class InitMotCle {
    
    @EJB
    private MotCleManager motCleManager;
    
    @PostConstruct
    private void initMotCle() {
        if (motCleManager.nbMotCle()== 0) {
            MotCle motCle1 = new MotCle("Windows 10");
            MotCle motCle2 = new MotCle("Linux");
            MotCle motCle3 = new MotCle("Debian");
            MotCle motCle4 = new MotCle("OSX");
            MotCle motCle5 = new MotCle("CentOS");
            MotCle motCle6 = new MotCle("Windows Server");
            MotCle motCle7 = new MotCle("Ubuntu Server");
            motCleManager.creer(motCle1);
            motCleManager.creer(motCle2);
            motCleManager.creer(motCle3);
            motCleManager.creer(motCle4);
            motCleManager.creer(motCle5);
            motCleManager.creer(motCle6);
            motCleManager.creer(motCle7);
        }
    }
}
