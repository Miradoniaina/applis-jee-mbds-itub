/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.jsf.backingbean;

import applisjee.mbds.itub.ejb.entity.Utilisateur;
import applisjee.mbds.itub.ejb.entity.UtilisateurRole;
import applisjee.mbds.itub.ejb.session.UtilisateurManager;
import applisjee.mbds.itub.ejb.session.UtilisateurRoleManager;
import applisjee.mbds.itub.jsf.util.Util;
import applisjee.mbds.itub.session.services.UtilisateurServices;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author Mirado
 */
@Named(value = "modificationUtilisateurMBean")
@ViewScoped
public class ModificationUtilisateurMBean implements Serializable {

    @EJB
    private UtilisateurManager utilisateurManager;
    
    @EJB
    private UtilisateurRoleManager utilisateurRoleManager;
    
    @EJB
    private UtilisateurServices us;

    private String pseudo;
    
    private String motDePasse;
    
    private Utilisateur utilisateur;

    /**
     *
     * @return
     */
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    /**
     *
     * @param utilisateur
     */
    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    /**
     *
     * @return
     */
    public String getPseudo() {
        return pseudo;
    }

    /**
     *
     * @param pseudo
     */
    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    /**
     *
     * @return
     */
    public String getMotDePasse() {
        return motDePasse;
    }

    /**
     *
     * @param motDePasse
     */
    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    /**
     * charger l'utilisaateur 
     */
    public void chargerUtilisateur() {
        utilisateur = utilisateurManager.Find(pseudo);
    }
    
    /**
     * Retourne la liste de tous les Role utilisateurs
     *
     * @return
     */
    public List<UtilisateurRole> getUtilisateurRoles() {
        List<UtilisateurRole> ur = new ArrayList<>();
        ur.add(new UtilisateurRole(this.utilisateur.getPseudo(), "utilisateur"));
        ur.add(new UtilisateurRole(this.utilisateur.getPseudo(), "admin"));
        
        return ur;
    }
    
    /**
     *
     * @return
     */
    public String enregistrer() {
//        utilisateurRoleManager.persist(utilisateur.getUtilisateurRole());
        us.modifierUtilisateur(utilisateur);
        Util.addFlashInfoMessage("Utilisateur " + utilisateur.getPseudo() + " modifier ");
        return "/admin/utilisateur/index?faces-redirect=true";
    }
}
