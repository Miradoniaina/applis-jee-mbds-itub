/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.jsf.backingbean;

import applisjee.mbds.itub.ejb.entity.MotCle;
import applisjee.mbds.itub.ejb.entity.Question;
import applisjee.mbds.itub.ejb.entity.Utilisateur;
import applisjee.mbds.itub.ejb.session.MotCleManager;
import applisjee.mbds.itub.ejb.session.QuestionManager;
import applisjee.mbds.itub.ejb.session.ReponseManager;
import applisjee.mbds.itub.ejb.session.UtilisateurManager;
import applisjee.mbds.itub.jsf.util.Util;
import java.io.Serializable;
import java.util.Date;
import javax.ejb.EJB;
import javax.faces.context.ExternalContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Andrianina_pc
 */
@Named(value = "questionBean")
@ViewScoped
public class QuestionBean implements Serializable {

    @EJB
    private UtilisateurManager utilisateurManager;

    @EJB
    private QuestionManager questionManager;
    
    @EJB
    private MotCleManager motCleManager;

    @Inject
    private ExternalContext externalContext;

    private final Question question = new Question();

    /**
     *
     * @return
     */
    public Question getQuestion() {
        return question;
    }

    /**
     * 
     * Rempli les attributs de l'objet question 
     * 
     * récupère l'utilisateur connecter pour l'attribut utilisateur de question
     * 
     * save question dans la bdd
     * 
     * save le mot clé dans la table motcle pour la fonctionnalité recherche
     * 
     * sort le popup pour signifier a l'utilisateur que la question a été crée
     * 
     * redirige vers l'accueil
     * 
     * @return
     */
    public String poserQuestion() {
        question.setDateQuestion(new Date());
        question.setNotifier(0);
        question.setResolu(false);

        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        String pseudo = request.getUserPrincipal().getName();
        Utilisateur userConnecter = utilisateurManager.Find(pseudo);
        question.setUtilisateur(userConnecter);
        
        questionManager.creer(question);
        
        for(String motCle : question.getAllMotCle()){
            motCleManager.creer(new MotCle(motCle));
        }
        

        Util.addFlashInfoMessage("Question " + question.getQuestion()
                + " créé");

        return "/accueil?faces-redirect=true";
    }

}
