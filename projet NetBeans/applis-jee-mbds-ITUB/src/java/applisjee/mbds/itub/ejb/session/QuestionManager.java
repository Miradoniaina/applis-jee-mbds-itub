/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.ejb.session;

import applisjee.mbds.itub.ejb.entity.Question;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Mirado
 */
@Stateless
public class QuestionManager {

    @PersistenceContext(unitName = "applis-jee-mbds-ITUBPU")
    private EntityManager em;

    /**
     *
     * @param question
     */
    public void creer(Question question) {
        em.persist(question);
    }

    /**
     *
     * @param question
     */
    public void marquerResolu(Question question) {
        question.setResolu(Boolean.TRUE);
        em.merge(question);
    }

    /**
     *
     * @return
     */
    public long nbQuestion() {
        Query q = em.createQuery("select count(q) from Question q");
        return (long) q.getSingleResult();
    }

    /**
     *
     * @param pseudo
     * @return
     */
    public long nbQuestion(String pseudo) {
        Query q = em.createQuery("select count(q) from Question q where q.utilisateur.pseudo = '" + pseudo + "'");
        return (long) q.getSingleResult();
    }

    /**
     *
     * @return
     */
    public List<Question> findAll() {
        Query q = em.createNamedQuery("Question.findAll");
        return q.getResultList();
    }

    /**
     *
     * @param idQuestion
     * @return
     */
    public Question findById(long idQuestion) {
        return em.find(Question.class, idQuestion);
    }

    /**
     * incrémenter la notification d'une question quand un autre utilisateur
     * ajoute une réponse
     *
     *
     * @param questionId
     */
    public void incrementeQuestionNotifer(long questionId) {
        Question question = this.findById(questionId);
        question.setNotifier(question.getNotifier() + 1);
        em.merge(question);
    }

    /**
     * rechercher une question à partir de(s) motclé(s) et/ou titre et/ou
     * contenu
     *
     *
     * @param motcle
     * @param texte
     * @return
     */
    public List<Question> search(String motcle, String texte) {
        String requette = "SELECT q FROM Question q WHERE 1 = 1";
        if (motcle.length() > 0) {
            String[] motcles = null;
            if (motcle.contains(";")) {
                motcles = motcle.split(";");
            } else {
                motcles = new String[1];
                motcles[0] = motcle;
            }
            for (String motcle1 : motcles) {
                requette += " and lower(q.motcle) like '%" + motcle1.toLowerCase() + "%'";
            }
        }
        if (texte.length() > 0) {
            String[] textes = null;
            if (texte.contains(" ")) {
                textes = texte.split(" ");
            } else {
                textes = new String[1];
                textes[0] = texte;
            }
            for (String texte1 : textes) {
                requette += " and (lower(q.titre) like '%" + texte1.toLowerCase() + "%'";
                requette += " or lower(q.question) like '%" + texte1.toLowerCase() + "%')";
            }
        }
        Query q = em.createQuery(requette);
        return q.getResultList();
    }
}
