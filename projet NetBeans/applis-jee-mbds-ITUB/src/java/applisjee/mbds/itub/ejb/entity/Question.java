/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.ejb.entity;

import applisjee.mbds.itub.util.Util;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mirado
 */
@Entity
@Table(name = "QUESTION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Question.findAll", query = "SELECT q FROM Question q order by q.questionId DESC"),
    @NamedQuery(name = "Question.findById", query = "SELECT q FROM Question q WHERE q.questionId = :questionId"),
    @NamedQuery(name = "Question.findByQuestion", query = "SELECT q FROM Question q WHERE q.question = :question"), //    @NamedQuery(name = "Customer.findByAddressline1", query = "SELECT c FROM Customer c WHERE c.addressline1 = :addressline1"),
//    @NamedQuery(name = "Customer.findByAddressline2", query = "SELECT c FROM Customer c WHERE c.addressline2 = :addressline2"),
//    @NamedQuery(name = "Customer.findByCity", query = "SELECT c FROM Customer c WHERE c.city = :city"),
//    @NamedQuery(name = "Customer.findByState", query = "SELECT c FROM Customer c WHERE c.state = :state"),
//    @NamedQuery(name = "Customer.findByPhone", query = "SELECT c FROM Customer c WHERE c.phone = :phone"),
//    @NamedQuery(name = "Customer.findByFax", query = "SELECT c FROM Customer c WHERE c.fax = :fax"),
//    @NamedQuery(name = "Customer.findByEmail", query = "SELECT c FROM Customer c WHERE c.email = :email"),
//    @NamedQuery(name = "Customer.findByCreditLimit", query = "SELECT c FROM Customer c WHERE c.creditLimit = :creditLimit")
})
public class Question implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "QUESTION_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long questionId;

    @Column(name = "TITRE")
    private String titre;

    @Lob
    @Column(name = "QUESTION")
    private String question;

    @Column(name = "DATE_QUESTION")
    @Temporal(TemporalType.DATE)
    private Date dateQuestion;

    @Column(name = "NOTIFIER")
    private Integer notifier;

    @Column(name = "RESOLU")
    private Boolean resolu;

    @Column(name = "MOTCLE")
    private String motcle;

    @JoinColumn(name = "UTILISATEUR", referencedColumnName = "PSEUDO")
    @ManyToOne(optional = false)
    private Utilisateur utilisateur;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "question")
    private Collection<Reponse> reponsesCollection;

    @Version
    private int version;

    /**
     * constructeur
     */
    public Question() {
    }

    /**
     * @param titre
     * @param question
     * @param dateQuestion
     * @param notifier
     * @param utilisateur
     */
    public Question(String titre, String question, Date dateQuestion, Integer notifier, Utilisateur utilisateur) {
        this.titre = titre;
        this.question = question;
        this.dateQuestion = dateQuestion;
        this.notifier = notifier;
        this.utilisateur = utilisateur;
    }

    /**
     * constructeur
     * @param id id
     * @param titre titre
     * @param question question 
     * @param dateQuestion date de la question
     * @param notifier combien la question est notifiée
     * @param utilisateur l'utilisateur
     */
    public Question(long id, String titre, String question, Date dateQuestion, Integer notifier, Utilisateur utilisateur) {
        this.questionId = id;
        this.titre = titre;
        this.question = question;
        this.dateQuestion = dateQuestion;
        this.notifier = notifier;
        this.utilisateur = utilisateur;
    }
    
    /**
     * @param titre
     * @param question
     * @param dateQuestion
     * @param notifier
     * @param utilisateur
     * @param motCle
     */
    public Question(String titre, String question, Date dateQuestion, Integer notifier, String motCle, Utilisateur utilisateur) {
        this.titre = titre;
        this.question = question;
        this.dateQuestion = dateQuestion;
        this.notifier = notifier;
        this.motcle = motCle;
        this.utilisateur = utilisateur;
    }

    /**
     * getter titre
     *
     * @return titre
     *
     */
    public String getTitre() {
        return titre;
    }

    /**
     * setter titre
     * @param titre le titre du question
     */
    public void setTitre(String titre) {
        this.titre = titre;
    }

    /**
     * getter questionId
     * @return questionId
     *
     */
    public Long getQuestionId() {
        return questionId;
    }

    /**
     * setter questionId
     * @param questionId questionId
     */
    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    /**
     * questionId question
     * @return question 
     */
    public String getQuestion() {
        return question;
    }

    /**
     * setter question
     * @param question la question
     */
    public void setQuestion(String question) {
        this.question = question;
    }

    /**
     * getter dateQuestion
     * @return la date de la question
     */
    public Date getDateQuestion() {
        return dateQuestion;
    }

    /**
     * setter dateQuestion
     * @param dateQuestion la date de la question
     */
    public void setDateQuestion(Date dateQuestion) {
        this.dateQuestion = dateQuestion;
    }

    /**
     *
     * @return la date de la question formatté
     */
    public String getDateFormat() {
        return Util.formatDate(dateQuestion);
    }

    /**
     *
     * @return le temps écoulé depuis la poste de la question
     */
    public String getTempEcoule() {
        String ret = "";
        LocalDate now = LocalDate.now();
        LocalDate lDateQuestion = this.dateQuestion.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();

        Period period = Period.between(now, lDateQuestion);
        int diff = period.getDays();

        int year = period.getYears();
        if (period.getYears() > 0) {
            ret += year + " an(s)";
        }

        int month = period.getMonths();
        if (month > 0) {
            ret += " " + month + " mois";
        }

        int jour = period.getDays();
        if (jour >= 0) {
            ret += " " + jour + " jour(s)";
        }

        return ret;
    }

    /**
     * getter notifier pour le nombre de notification pour la question
     * @return notifier le nombre de fois que la question a été notifié
     */
    public Integer getNotifier() {
        return notifier;
    }

    /**
     * setter notifier pour le nombre de notification pour la question
     * @param notifier le nombre de fois que la question a été notifié
     */
    public void setNotifier(Integer notifier) {
        this.notifier = notifier;
    }

    /**
     *
     * @return si la question est résolue ou non
     */
    public Boolean getResolu() {
        return resolu;
    }

    /**
     *
     * @param resolu si la question est résolue ou non
     */
    public void setResolu(Boolean resolu) {
        this.resolu = resolu;
    }

    /**
     *
     * @return le mot clé 
     */
    public String getMotcle() {
        return motcle;
    }

    /**
     *
     * @param motcle le mot clé
     */
    public void setMotcle(String motcle) {
        this.motcle = motcle;
    }

    /**
     * build list of key word by attribute motcle
     * @return tous les mots clés
     */
    public String[] getAllMotCle() {
        String[] ret = null;
        if(this.motcle != null) ret = this.motcle.split(";");
        return ret;
    }

    /**
     *
     * @return l'utilisateur
     */
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    /**
     * setter Utilisateur
     *
     * @param utilisateur l'utilisateur
     */
    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    /**
     * getter version
     *
     * @return la version pour les modification de la question pour éviter la concurrence
     */
    public int getVersion() {
        return version;
    }

    /**
     * setter version
     *
     * @param version la version pour les modification de la question pour éviter la concurrence
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * getter liste des reponses du question
     *
     * @return liste des reponses du question
     */
    public Collection<Reponse> getReponsesCollection() {
        return reponsesCollection;
    }

    /**
     * setter liste des reponses du question
     *
     * @param reponsesCollection liste des reponses du question
     */
    public void setReponsesCollection(Collection<Reponse> reponsesCollection) {
        this.reponsesCollection = reponsesCollection;
    }
}
