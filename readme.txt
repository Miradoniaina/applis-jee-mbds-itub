=======================================================================================
				PCHelp
	Application for assistance in the development and administration of computers.

=======================================================================================

Overview
========
A "Logbook" Web to find solutions already found for a group of computer scientists
in a particular community.
The app was written in JAVA (WEB type JAVA EE), JPA and PAYARA while it is fully built
on Java 1.8.

Getting Started
===============
Here is a very short description for the contents of the global file, which gives the necessary information
to launch the application from the global zip file.

After unzipping the file, we have 2 important files:
- documentation : which contains the javadoc in a javadoc subdirectory;
		  A user guide named � guide-utilisateur.pdf �;
		  A developer's guide named � guide-programmation.pdf �;
		  A bugs.pdf file for bugs spotted in the application;
		  A file quiafaitquoi.pdf which exposes the facts of the members of the group.
- projet NetBeans : which contains all the files in the NetBeans project.
This directory contains a zip file obtained by the export of the project (menu File> Export Project of NetBeans).


Launch the application
======================
Here is the procedure for testing the application on your machine.
Step 1: import the file applis-jee-mbds-ITUB.zip in \projet NetBeans\zip on netbeans
=> File > Import Project > From ZIP...
Step 2: Add the PrimeFaces-7.0 library in \applis-jee-mbds-ITUB\lib from the zipped project folder
=> right click on the project in netbeans> property> category library
Step 3: Creating the data source with Java DB
=> service tab> right click on Java Db> Create Database
* Database Name: applis-jee-mbds-ITUB
* UserName: app
* Password: app
Step 4: Launch the application on netbeans


Exploring the Application
=========================
To test the application we created 4 users when the application was initialized.
For the administration of the site (01 user):
	- username = admin
	- password = itu
For a standard use of the site (03 user):
	- username = miradoniaina
	- password = itu
	-------------------------
	- username = layton
	- password = itu
	-------------------------
	- username = mbola
	- password = itu