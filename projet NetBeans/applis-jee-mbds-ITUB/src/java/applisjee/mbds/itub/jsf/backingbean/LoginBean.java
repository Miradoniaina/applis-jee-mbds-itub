/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.jsf.backingbean;

import applisjee.mbds.itub.ejb.entity.Utilisateur;
import applisjee.mbds.itub.session.services.UtilisateurServices;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.security.enterprise.AuthenticationStatus;
import static javax.security.enterprise.AuthenticationStatus.SEND_CONTINUE;
import static javax.security.enterprise.AuthenticationStatus.SEND_FAILURE;
import javax.security.enterprise.SecurityContext;
import static javax.security.enterprise.authentication.mechanism.http.AuthenticationParameters.withParams;
import javax.security.enterprise.credential.Credential;
import javax.security.enterprise.credential.Password;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.primefaces.model.UploadedFile;

/**
 * Backing bean pour la page de login avec un formulaire personnalisé.
 *
 * @author Mirado
 */
@Named(value = "loginBean")
@ViewScoped
public class LoginBean implements Serializable {

    @Inject
    private SecurityContext securityContext;
    @Inject
    private FacesContext facesContext;
    @Inject
    private ExternalContext externalContext;
    
    @EJB
    private UtilisateurServices utilisateurServices;

    private Utilisateur utilisateur = new Utilisateur();

    private String confMotDePasse;

    private UploadedFile file;

    /**
     * vérifie via ajax si le pseudo pour une inscription n'est pas déjà
     * utilisé par un compte
     *
     * @return
     */
    public String getSiPseudoDejaUtiliseAjax() {
        if ((this.utilisateur.getPseudo() != null) && (this.utilisateurServices.nbPseudo(this.utilisateur.getPseudo()) != 0)) {
            return "pseudo déjà assigné à un compte.";
        }
        return null;
    }

    /**
     * authentification de l'utilisateur
     */
    public void login() {
        Credential credential
                = new UsernamePasswordCredential(utilisateur.getPseudo(), new Password(utilisateur.getMotDePasse()));
        AuthenticationStatus status = securityContext.authenticate(
                (HttpServletRequest) externalContext.getRequest(),
                (HttpServletResponse) externalContext.getResponse(),
                withParams().credential(credential));
        if (status.equals(SEND_CONTINUE)) {
            facesContext.responseComplete();
        } else if (status.equals(SEND_FAILURE)) {
            addError(facesContext, "Pseudo / mot de passe incorrects");
        }
    }

    /**
     * inscription de l'utilisateur
     *
     * @return
     */
    public String inscription() {
        try {
            this.utilisateurServices.inscription(this.utilisateur, this.confMotDePasse);
            return "/connect/index?faces-redirect=true";
        } catch (EJBException ejbex) {
            addError(facesContext, ejbex.getMessage());
            return "/connect/auth/inscription?faces-redirect=true";
        }
    }

    Byte[] toObjects(byte[] bytesPrim) {

        Byte[] bytes = new Byte[bytesPrim.length];
        int i = 0;
        for (byte b : bytesPrim) {
            bytes[i++] = b; //Autoboxing
        }
        return bytes;

    }

    /**
     * Ajoute une erreur à afficher dans la page de login.
     *
     * @param facesContext
     * @param authentication_failed
     */
    private void addError(FacesContext facesContext,
            String message) {
        facesContext.addMessage(
                null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        message,
                        null));
    }

    /**
     *
     * @return
     */
    public String logout() {
        try {
            HttpServletRequest httpServletRequest = (HttpServletRequest) externalContext.getRequest();
            httpServletRequest.logout();
            httpServletRequest.getSession().invalidate();
        } catch (ServletException ex) {
            System.err.println("Erreur pendant logout : " + ex);
        }
        return "/accueil?faces-redirect=true"; // reste sur la page index.xhtml
    }
    
//    GETTERS SETTERS

    /**
     *
     * @return
     */
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    /**
     *
     * @param utilisateur
     */
    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    /**
     *
     * @return
     */
    public String getConfMotDePasse() {
        return confMotDePasse;
    }

    /**
     *
     * @param confMotDePasse
     */
    public void setConfMotDePasse(String confMotDePasse) {
        this.confMotDePasse = confMotDePasse;
    }

    /**
     *
     * @return
     */
    public UploadedFile getFile() {
        return file;
    }

    /**
     *
     * @param file
     */
    public void setFile(UploadedFile file) {
        this.file = file;
    }
}
