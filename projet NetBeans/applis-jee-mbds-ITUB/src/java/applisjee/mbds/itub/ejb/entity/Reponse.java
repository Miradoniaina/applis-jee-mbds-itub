/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.ejb.entity;

import applisjee.mbds.itub.util.Util;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mirado
 */
@Entity
@Table(name = "REPONSE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reponse.findAll", query = "SELECT r FROM Reponse r"),
    @NamedQuery(name = "Reponse.findByIdQuestion", query = "SELECT r FROM Reponse r WHERE r.question.questionId = :questionId"), //    @NamedQuery(name = "Customer.findByAddressline1", query = "SELECT c FROM Customer c WHERE c.addressline1 = :addressline1"),
//    @NamedQuery(name = "Customer.findByAddressline2", query = "SELECT c FROM Customer c WHERE c.addressline2 = :addressline2"),
//    @NamedQuery(name = "Customer.findByCity", query = "SELECT c FROM Customer c WHERE c.city = :city"),
//    @NamedQuery(name = "Customer.findByState", query = "SELECT c FROM Customer c WHERE c.state = :state"),
//    @NamedQuery(name = "Customer.findByPhone", query = "SELECT c FROM Customer c WHERE c.phone = :phone"),
//    @NamedQuery(name = "Customer.findByFax", query = "SELECT c FROM Customer c WHERE c.fax = :fax"),
//    @NamedQuery(name = "Customer.findByEmail", query = "SELECT c FROM Customer c WHERE c.email = :email"),
//    @NamedQuery(name = "Customer.findByCreditLimit", query = "SELECT c FROM Customer c WHERE c.creditLimit = :creditLimit")
})
public class Reponse implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "REPONSE_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long ReponseId;

    @Lob
    @Column(name = "REPONSE")
    private String reponse;

    @Column(name = "DATE_REPONSE")
    @Temporal(TemporalType.DATE)
    private Date dateReponse;

    @Column(name = "VOTE")
    private Integer vote;

    @Column(name = "VUE")
    private Boolean vue;

    @JoinColumn(name = "UTILISATEUR", referencedColumnName = "PSEUDO")
    @ManyToOne(optional = false)
    private Utilisateur utilisateur;

    @JoinColumn(name = "QUESTION", referencedColumnName = "QUESTION_ID")
    @ManyToOne(optional = false)
    private Question question;

    @Version
    private int version;

    /**
     * constructeur
     */
    public Reponse() {

    }

    /**
     *
     * @param reponse
     * @param dateReponse
     * @param vote
     * @param vue
     * @param utilisateur
     * @param question
     */
    public Reponse(String reponse, Date dateReponse, Integer vote, Boolean vue, Utilisateur utilisateur, Question question) {
        this.reponse = reponse;
        this.dateReponse = dateReponse;
        this.vote = vote;
        this.vue = vue;
        this.utilisateur = utilisateur;
        this.question = question;
    }

    /**
     *
     * @return
     */
    public Long getReponseId() {
        return ReponseId;
    }

    /**
     *
     * @param ReponseId
     */
    public void setReponseId(Long ReponseId) {
        this.ReponseId = ReponseId;
    }

    /**
     *
     * @return
     */
    public String getReponse() {
        return reponse;
    }

    /**
     *
     * @param reponse
     */
    public void setReponse(String reponse) {
        this.reponse = reponse;
    }

    /**
     *
     * @return
     */
    public Date getDateReponse() {
        return dateReponse;
    }

    /**
     *
     * @param dateReponse
     */
    public void setDateReponse(Date dateReponse) {
        this.dateReponse = dateReponse;
    }

    /**
     *
     * @return
     */
    public String getDateFormat() {
        return Util.formatDate(dateReponse);
    }

    /**
     *
     * recupère le temps écoulé depuis la date de creation de la réponse
     *
     * @return
     */
    public String getTempEcoule() {
        String ret = "";
        if (this.dateReponse != null) {
            LocalDate now = LocalDate.now();
            LocalDate lDateQuestion = this.dateReponse.toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();

            Period period = Period.between(lDateQuestion, now);
            int diff = period.getDays();

            int year = period.getYears();
            if (period.getYears() > 0) {
                ret += year + " an(s)";
            }

            int month = period.getMonths();
            if (month > 0) {
                ret += " " + month + " mois";
            }

            int jour = period.getDays();
            if (jour >= 0) {
                ret += " " + jour + " jour(s)";
            }
        }

        return ret;
    }

    /**
     *
     * @return
     */
    public Integer getVote() {
        return vote;
    }

    /**
     *
     * @param vote
     */
    public void setVote(Integer vote) {
        this.vote = vote;
    }

    /**
     *
     * @return
     */
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    /**
     *
     * @param utilisateur
     */
    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    /**
     *
     * @return
     */
    public Question getQuestion() {
        return question;
    }

    /**
     *
     * @param question
     */
    public void setQuestion(Question question) {
        this.question = question;
    }

    /**
     *
     * @return
     */
    public int getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     *
     * @return
     */
    public Boolean getVue() {
        return vue;
    }

    /**
     *
     * @return
     */
    public String getVueStr() {
        if (vue) {
            return "";
        }
        
        return "(non vue)";
    }

    /**
     *
     * @param vue
     */
    public void setVue(Boolean vue) {
        this.vue = vue;
    }
}
