/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.ejb.session;

import applisjee.mbds.itub.ejb.entity.Utilisateur;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Mirado
 */
@Stateless
public class UtilisateurManager {

    @PersistenceContext(unitName = "applis-jee-mbds-ITUBPU")
    private EntityManager em;
    
    /**
     *
     * @param object
     */
    public void persist(Object object) {
        em.persist(object);
    }
    
    /**
     *
     * @return
     */
    public List<Utilisateur> findAll() {
        Query q = em.createNamedQuery("Utilisateur.findAll");
        return q.getResultList();
    }
    
    /**
     *
     * @param pseudo
     * @return
     */
    public Utilisateur Find(String pseudo) {
        return em.find(Utilisateur.class, pseudo);
    }
    
    /**
     *
     * @param u
     * @return
     */
    public Utilisateur update(Utilisateur u) {
        return em.merge(u);
    }
    
    /**
     *
     * @param u
     */
    public void supprimerCompte(Utilisateur u) {
         em.remove(em.merge(u));
    }
}
