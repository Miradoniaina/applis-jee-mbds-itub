/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.jsf.backingbean;

import applisjee.mbds.itub.ejb.entity.Question;
import applisjee.mbds.itub.ejb.session.NotificationsManager;
import applisjee.mbds.itub.ejb.session.QuestionManager;
import applisjee.mbds.itub.jsf.internationalisation.Internationalisation;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Mirado
 */
@Named(value = "notificationMBean")
@RequestScoped
public class NotificationMBean {

    @Inject
    private ExternalContext externalContext;

    @EJB
    private NotificationsManager notificationsManager;

    @EJB
    private QuestionManager questionManager;

    @Inject
    private Internationalisation internationalisation;

    /**
     * Fonction qui retourne le nombre de notifications de l'utilisateur
     *
     * @return String
     */
    public String getNbNotification() {
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        String pseudo = request.getUserPrincipal().getName();
        
        if (questionManager.nbQuestion(pseudo) != 0) {
            return notificationsManager.notificationTotal(pseudo).toString();
        }

        return "0";
    }

    /**
     * Fonction qui retourne une liste des questions notifié
     *
     * @return 
     */
    public List<Question> getQuestionNotifie() {
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        return notificationsManager.questionNotifie(request.getUserPrincipal().getName());
    }

}
