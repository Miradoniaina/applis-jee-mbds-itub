/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.util;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author Andrianina_pc
 */

public class Util {
   
    /**
     * 
     * @param d
     * @return 
     */
    public static String formatDate(Date d) {
        String result = "";
        if(d != null) {
            Format f = new SimpleDateFormat("E dd MMM yyyy HH:mm:ss");
            result = f.format(d);
        }
        return result;
    }
}
