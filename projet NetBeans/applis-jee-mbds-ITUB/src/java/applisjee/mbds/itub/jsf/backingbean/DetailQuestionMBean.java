/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.jsf.backingbean;

import applisjee.mbds.itub.ejb.entity.Question;
import applisjee.mbds.itub.ejb.entity.Reponse;
import applisjee.mbds.itub.ejb.session.QuestionManager;
import applisjee.mbds.itub.ejb.session.ReponseManager;
import applisjee.mbds.itub.ejb.session.UtilisateurManager;
import applisjee.mbds.itub.jsf.util.Util;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PostRemove;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Mirado
 */
@Named(value = "detailQuestionMBean")
@ViewScoped
public class DetailQuestionMBean implements Serializable {

    private Long questionId;
    private Question question;

    @EJB
    private UtilisateurManager utilisateurManager;

    @EJB
    private QuestionManager questionManager;

    @EJB
    private ReponseManager reponseManager;

    @Inject
    private ExternalContext externalContext;

    /**
     *
     * @return
     */
    public Long getQuestionId() {
        return questionId;
    }

    /**
     *
     * @return
     */
    public Question getQuestion() {
        return question;
    }

    /**
     *
     * @param question
     */
    public void setQuestion(Question question) {
        this.question = question;
    }

    /**
     *
     * @param questionId
     */
    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    /**
     * charger la question d'id questionId
     */
    public void loadQuestion() {
        if (question == null) {
            question = questionManager.findById(questionId);
            question.setReponsesCollection(reponseManager.findByIdQuestion(questionId));
        }
    }

    /**
     * charger la question notifiées
     */
    public void loadQuestionNotification() {
        this.loadQuestion();
        reponseManager.miseAJourReponsesVue(new Long(questionId));
    }
    
    /**
     *
     * @param question
     */
    public void resolu(Question question) {
        questionManager.marquerResolu(question);
        Util.addFlashInfoMessage("Marquer comme résolu");
    }

    /**
     *
     * @param idReponse
     * @return
     */
    public String votePlus(Long idReponse) {
        return voter(idReponse, "plus");
    }

    /**
     *
     * @param idReponse
     * @return
     */
    public String voteMoins(Long idReponse) {
        return voter(idReponse, "moins");
    }

    /**
     *
     * On recupere la réponse
     *
     * On verifie si l'ulitisateur est connecté Si oui on vote Sinon il doit se
     * connecté
     *
     * Avant de voter on verifie s'il a déjà voté Sinon on affiche un message
     * d'erreur
     *
     * on incrémente ou décrémente l'attribut vote de la réponse on informe
     * l'utilisateur du changement effectué
     *
     * on sauvegarde la modification
     *
     * on reload la page
     *
     * @param idReponse
     * @param type
     * @return
     */
    private String voter(Long idReponse, String type) {
        Reponse rep = reponseManager.findById(idReponse);

        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        if (request.getUserPrincipal() != null) {
            String pseudo = request.getUserPrincipal().getName();
            
            if (reponseManager.peutVoter(idReponse, pseudo)) {
                reponseManager.aVoter(idReponse, pseudo);
                if (type.equals("plus")) {
                    rep.setVote(rep.getVote() + 1);
                    Util.addFlashInfoMessage("Vote incrémenté");
                } else {
                    rep.setVote(rep.getVote() - 1);
                    Util.addFlashInfoMessage("Vote decrémenté");
                }
            }else{
                Util.addFlashInfoMessage("Vous avez déjà voté sur cette reponse");
                return "/connect/detail_question?idQuestion=" + rep.getQuestion().getQuestionId() + "faces-redirect=true";
            }

            reponseManager.update(rep);

            return "/detail_question?idQuestion=" + rep.getQuestion().getQuestionId() + "faces-redirect=true";
        } else {
            Util.addFlashInfoMessage("Connecté vous pour voté");
            return "/connect/detail_question?idQuestion=" + rep.getQuestion().getQuestionId() + "faces-redirect=true";

        }
    }

}
