package applisjee.mbds.itub.ejb.session.init;

import applisjee.mbds.itub.ejb.session.QuestionManager;
import applisjee.mbds.itub.ejb.session.UtilisateurManager;
import applisjee.mbds.itub.util.HashMdp;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.sql.DataSource;

/**
 * Initialise les données pour les utilisateurs : création des tables et
 *
 * insertion des données.
 *
 * @author Mirado
 * @version 1.0
 */
@Singleton
@LocalBean
@Startup
@DataSourceDefinition(
        className = "org.apache.derby.jdbc.ClientDataSource",
        name = "java:app/jdbc/applis-jee-mbds-ITUB",
        serverName = "localhost",
        portNumber = 1527,
        user = "app", // nom et
        password = "app", // mot de passe que vous avez donnés lors de la création de la base de données
        databaseName = "applis-jee-mbds-ITUB"
)
public class Init {

    @Resource(lookup = "java:app/jdbc/applis-jee-mbds-ITUB")
    private DataSource dataSource;

    /**
     * Le "HashMdp" Pour coder le mot de passe
     *
     * @see HashMdp
     */
    @Inject
    private HashMdp passwordHash;

    @EJB
    private UtilisateurManager utilisateurManager;

    @EJB
    private QuestionManager questionManager;

    /**
     * création des tables et insertion des données lors du démarrage
     *
     */
    @PostConstruct
    public void init() {
        initUtilisateur();
    }
    
    /**
     * Méthode pour initialiser les utilisateurs et leur roles
     * 
     */
    private void initUtilisateur() {
        try (Connection c = dataSource.getConnection()) {
            // Si la table des logins n'existe pas déjà, créer les tables
            if (!existe(c, "UTILISATEUR")) { // Attention, la casse compte !!!
                System.out.println("Création des tables");
                // Remarque : la table SEQUENCE est créée automatiquement ; pas besoin de la créer
                execute(c, "CREATE TABLE utilisateur (\n"
                        + "	pseudo VARCHAR(125) PRIMARY KEY,\n"
                        + "	email VARCHAR(100),\n"
                        + "	nom VARCHAR(20), \n"
                        + "	prenom VARCHAR(20), \n"
                        + "	date_de_naissance date,\n"
                        + "	photo BLOB,\n"
                        + "	mot_de_passe VARCHAR(160)\n"
                        + ")");

                execute(c, "CREATE TABLE utilisateur_role (\n"
                        + "	pseudo VARCHAR(125) PRIMARY KEY references utilisateur, \n"
                        + "	role VARCHAR(20)\n"
                        + ")");
            } else {
                System.out.println("Tables existent déjà");
            }

            // Si la table LOGIN n'est pas vide, ne rien faire
            if (vide(c, "utilisateur")) {
                System.out.println("============ Table utilisateur vide ; initialisation des données dans ls tables");
                // La table utilisateur est vide
                // Le mot de passe haché :
                String hashMdp = passwordHash.generate("itu");

                // insertion utilisateur
                execute(c, "INSERT INTO utilisateur \n"
                        + "( pseudo, email, nom, prenom, date_de_naissance, photo, mot_de_passe) \n"
                        + "VALUES\n"
                        + "('miradoniaina' ,'miradoeddy@gmail.com', 'RABETSIMANDRANTO', 'Miradoniaina Eddy', '3/02/1993', null, '" + hashMdp + "')"
                );

                execute(c, "INSERT INTO utilisateur \n"
                        + "( pseudo, email, nom, prenom, date_de_naissance, photo, mot_de_passe) \n"
                        + "VALUES\n"
                        + "('layton', 'rakotoandrianina@gmail.com', 'RAKOTOARISOA', 'Andrianina Layton', '2/05/1994', null, '" + hashMdp + "')"
                );

                execute(c, "INSERT INTO utilisateur \n"
                        + "( pseudo, email, nom, prenom, date_de_naissance, photo, mot_de_passe) \n"
                        + "VALUES\n"
                        + "('mbola', 'mbolainfo@gmail.com', 'RAZAFINDRAMIANDRA', 'Mbola', '2/06/1994', null, '" + hashMdp + "')"
                );

                execute(c, "INSERT INTO utilisateur \n"
                        + "(pseudo, email, nom, prenom, date_de_naissance, photo, mot_de_passe) \n"
                        + "VALUES\n"
                        + "('admin', 'admin@gmail.com', 'Admin', 'admin', '2/02/1993', null, '" + hashMdp + "')"
                );

//              insertion role utilisateur
                execute(c, "INSERT INTO utilisateur_role\n"
                        + "(pseudo, role)\n"
                        + " VALUES\n"
                        + " ('miradoniaina' , 'utilisateur')");
                execute(c, "INSERT INTO utilisateur_role\n"
                        + "(pseudo, role)\n"
                        + " VALUES\n"
                        + " ('layton', 'utilisateur')");
                execute(c, "INSERT INTO utilisateur_role\n"
                        + "(pseudo, role)\n"
                        + " VALUES\n"
                        + " ('mbola', 'utilisateur')");
                execute(c, "INSERT INTO utilisateur_role\n"
                        + "(pseudo, role)\n"
                        + " VALUES\n"
                        + " ('admin', 'admin')");
            }

        } catch (SQLException e) {
            // Une méthode annotée avec @PostConstruct ne peut lancer d'exception contrôlée.
            e.printStackTrace();
        }
    }
   

    private void execute(Connection c, String query) {
        try (PreparedStatement stmt = c.prepareStatement(query)) {
            stmt.executeUpdate();
        } catch (SQLException e) {
            // Pour les logs du serveur d'application
            e.printStackTrace();
        }
    }

    /**
     * Teste si une table existe déjà.
     *
     * @param connection
     * @param nomTable nom de la table ; attention la casse compte !
     * @return true ssi la table existe.
     * @throws SQLException
     */
    private static boolean existe(Connection connection, String nomTable)
            throws SQLException {
        boolean existe;
        DatabaseMetaData dmd = connection.getMetaData();
        try (ResultSet tables = dmd.getTables(connection.getCatalog(), null, nomTable, null)) {
            existe = tables.next();
        }
        return existe;
    }

    /**
     *
     * @return true ssi la table LOGIN est vide.
     */
    private boolean vide(Connection c, String nomTable) throws SQLException {
        Statement stmt = c.createStatement();
        ResultSet rset = stmt.executeQuery("select count(1) from " + nomTable);
        rset.next();
        int nb = rset.getInt(1);
        return nb == 0;
    }
}
