/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.jsf.internationalisation;

import javax.inject.Named;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

/**
 * Classe pour l'internationalisation de l'application implémenté gràce au
 * tutoriel https://www.mkyong.com/jsf2/jsf-2-internationalization-example/
 *
 * @author Mirado
 */
@Named(value = "internationalisation")
@SessionScoped
public class Internationalisation implements Serializable {

    private static final long serialVersionUID = 1L;

    private String localeCode = new String();

    private static Map<String, Object> countries;

    static {
        countries = new LinkedHashMap<String, Object>();
        countries.put("English", Locale.ENGLISH); //label, value
        countries.put("Francais", Locale.FRANCE);
    }

    /**
     * 
     * @return
     * map du langue pour l'internationalisation de l'applis 
     */
    public Map<String, Object> getCountriesInMap() {
        return countries;
    }

    /**
     *
     * @return
     * retourne la code de la langue
     */
    public String getLocaleCode() {
        return localeCode;
    }

    /**
     *
     * @param localeCode
     * setter la code de la langue "localCode"
     */
    public void setLocaleCode(String localeCode) {
        this.localeCode = localeCode;
    }

    //value change event listener

    /**
     *
     * @param e
     * ValueChangeEvent lors du changement de valeur
     */
    public void countryLocaleCodeChanged(ValueChangeEvent e) {

        String newLocaleValue = e.getNewValue().toString();

        //loop country map to compare the locale code
        for (Map.Entry<String, Object> entry : countries.entrySet()) {
            if (entry.getValue().toString().equals(newLocaleValue)) {
                FacesContext.getCurrentInstance()
                        .getViewRoot().setLocale((Locale) entry.getValue());
            }
        }
    }

    
    /**
     * application de la langue du navigateur par défaut si c'est en français. En anglais pour les autres langues non supporter 
     * 
     */
    @PostConstruct
    public void viewLangBrowser() {
        String browserLangage = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale().getLanguage();
        switch (browserLangage) {
            case "fr":
                this.localeCode = "fr_FR";
                break;
        }
    }

    /**
     * chargement de la langue avant l'affichage d'une page selon la langue définie
     * 
     */
    public void loadLang() {
        switch (this.localeCode) {
            case "fr_FR":
                FacesContext.getCurrentInstance()
                        .getViewRoot().setLocale(Locale.FRANCE);
                ;
                break;
            default:
                FacesContext.getCurrentInstance()
                        .getViewRoot().setLocale(Locale.ENGLISH);

        }
    }
}
