/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applisjee.mbds.itub.jsf.converter;

import applisjee.mbds.itub.ejb.entity.UtilisateurRole;
import applisjee.mbds.itub.ejb.session.UtilisateurManager;
import applisjee.mbds.itub.ejb.session.UtilisateurRoleManager;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Mirado
 */
@ApplicationScoped // Pour en faire un bean CDI
@FacesConverter(value = "converterRole", managed = true)
public class ConverterRole implements Converter<UtilisateurRole> {

    @EJB
    private UtilisateurRoleManager utilisateurRoleManager;
    
    /**
     *
     * @param context
     * @param component
     * @param value
     * @return
     */
    @Override
    public UtilisateurRole getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null) { // Requis par la spécification
            return null;
        }
        return utilisateurRoleManager.findByPseudo(value);
    }

    /**
     *
     * @param arg0
     * @param arg1
     * @param value
     * @return
     */
    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, UtilisateurRole value) {
        return value.getRole();
    }
    
}
